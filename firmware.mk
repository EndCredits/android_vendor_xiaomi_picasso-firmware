#Firmware Updates
PRODUCT_COPY_FILES += \
	vendor/xiaomi/picasso-firmware/devcfg.mbn:install/firmware-update/devcfg.mbn \
	vendor/xiaomi/picasso-firmware/hyp.mbn:install/firmware-update/hyp.mbn \
	vendor/xiaomi/picasso-firmware/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
	vendor/xiaomi/picasso-firmware/km4.mbn:install/firmware-update/km4.mbn \
	vendor/xiaomi/picasso-firmware/BTFM.bin:install/firmware-update/BTFM.bin \
	vendor/xiaomi/picasso-firmware/xbl.elf:install/firmware-update/xbl.elf \
	vendor/xiaomi/picasso-firmware/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
	vendor/xiaomi/picasso-firmware/featenabler.mbn:install/firmware-update/featenabler.mbn \
	vendor/xiaomi/picasso-firmware/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
	vendor/xiaomi/picasso-firmware/dspso.bin:install/firmware-update/dspso.bin \
	vendor/xiaomi/picasso-firmware/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
	vendor/xiaomi/picasso-firmware/tz.mbn:install/firmware-update/tz.mbn \
	vendor/xiaomi/picasso-firmware/aop.mbn:install/firmware-update/aop.mbn \
	vendor/xiaomi/picasso-firmware/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
	vendor/xiaomi/picasso-firmware/abl.elf:install/firmware-update/abl.elf \
	vendor/xiaomi/picasso-firmware/storsec.mbn:install/firmware-update/storsec.mbn \
	vendor/xiaomi/picasso-firmware/xbl_config.elf:install/firmware-update/xbl_config.elf